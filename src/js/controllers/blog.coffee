poetInitializer = require('../blog/poetInit')
db = require '../util/db'
poet = null


exports.init = (app, blog_routes) ->
  poetInitializer.start(app, blog_routes).then (p) ->
    poet = p
    routes poet

exports.byAuthor = (req, res) ->
  res.locals.currentUrl = req.protocol + '://' + req.get('host') + req.path
  author = db.getData().team.findById(req.params.author)
  page = getPage(poet.helpers.postsByAuthor(author.email), req)

  res.render 'pages/blog/author', {
    result: page
    url: poet.helpers.authorURL(req.params.author)
    author: author
  }


getPage = (list, req) ->
  pageNumber = req.query.page || 1
  total = list.length
  lastPost = pageNumber * poet.options.postsPerPage
  from = lastPost - poet.options.postsPerPage
  to = lastPost
  posts = list.slice(from, to)

  return {total: total, pageNumber: parseInt(pageNumber), pageCount: Math.ceil(total / poet.options.postsPerPage), items: posts}


routes = (poet) ->

  poet.addRoute('/blog/posts/:post', (req, res, next) ->
    post = poet.helpers.getPost(req.params.post)

    if post
      res.locals.currentUrl = req.protocol + '://' + req.get('host') + post.url
        
      res.render('pages/blog/post', { post: post })
    else
      next()
  )

  poet.addRoute('/blog', (req, res) ->
    res.locals.currentUrl = req.protocol + '://' + req.get('host') + req.path
    page = req.query.page || 1
    lastPost = page * poet.options.postsPerPage
    page = getPage(poet.helpers.getPosts(), req)

    res.render 'pages/blog/latest', {
      result: page
      url: poet.helpers.pageURL()
      pageCount: page.pageCount
    }
  )

  poet.addRoute('/blog/tags/:tag', (req, res) ->
    res.locals.currentUrl = req.protocol + '://' + req.get('host') + req.path
    tag = req.params.tag

    page = getPage(poet.helpers.postsWithTag(tag), req)

    res.render 'pages/blog/tag', {
      result: page
      url: poet.helpers.tagURL(tag)
      tag: tag
    }
  )

  poet.addRoute('/blog/categories/:category', (req, res) ->
    res.locals.currentUrl = req.protocol + '://' + req.get('host') + req.path
    cat = req.params.category
    page = getPage(poet.helpers.postsWithCategory(cat), req)

    res.render 'pages/blog/category', {
      result: page
      url: poet.helpers.categoryURL(cat)
      category: cat
    }
  )

