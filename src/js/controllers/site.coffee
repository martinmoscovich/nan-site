fs = require 'fs'
db = require '../util/db'
_ = require 'underscore'

PAGE_SIZE = 9


# Home

exports.home = (req, res) ->
  console.debug 'Home page requested'
  res.render 'pages/index', { title: 'NaN Labs' }


# About Us

exports.team = (req, res, next) ->
  console.debug 'Team page requested'
  res.render 'pages/team', { team: db.getData().team }


# Services

exports.services = (req, res, next) ->
  console.debug "Services page requested"
  res.render 'pages/services'


# Our Work

exports.projects = (req, res, next) ->
  req.query.api = req.query.api || false
  number = parseInt(req.query.page || 1)
  console.debug "Projects page #{number} requested"

  page = getPage(db.getData().projects, number)
  console.log(page)
  page.items = _.map(page.items, (p) -> {id: p.id, name: p.name, client: p.client})

  if req.query.api
    res.json(page)
  else
    res.render 'pages/project_list', { page: page }

getPage = (list, number) ->
  start = (number - 1) * PAGE_SIZE
  end = start + PAGE_SIZE

  items = list[start...end]
  return { items: items, next: list.length > end, number: number }

exports.projectDetails = (req, res, next) ->
  project = db.getById(db.getData().projects, parseInt(req.params.id))
  if !project
    next(404)
  else
    res.render 'pages/project_details', {project: project}
