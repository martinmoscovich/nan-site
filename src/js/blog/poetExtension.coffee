_ = require 'underscore'
moment = require 'moment'
db = require '../util/db'

exports.addDefinitions = (poet) ->

  ###
  # Calculates and returns the months where posts were made.
  ###
  poet.helpers.getBlogMonths = () ->
    if(poet.cache.months)
      console.debug "Months obtained from the cache"
      return poet.cache.months

    console.debug "Calculating posts months..."
    #return poet.cache.months = Object.keys(poet.cache.postsByDate).map (key) -> moment({year: key.substring(0,4), month: key.substring(4)})

    postToMonth = (post) -> moment({year: post.date.getFullYear(), month: post.date.getMonth()})
    momentEquals = (moment) -> moment.valueOf()

    return poet.cache.months = _.unique(poet.helpers.getPosts().map(postToMonth), momentEquals)

  ###
  # Finds and returns the articles posted on the provided month and year
  ###
  poet.helpers.postsWithMonthYear = (month, year) ->
    poet.cache.postByMonth = poet.cache.postByMonth || {}

    key = String(year) + ("00" + month).slice(-2)
    if(poet.cache.postByMonth[key])
      console.debug("Posts del mes %d-%d encontrados en la cache", month, year)
      return poet.cache.postByMonth[key]

    console.debug("Calculando posts del mes %d-%d...", month, year)
    return poet.cache.postByMonth[key] = poet.helpers.getPosts(poet).filter((post) -> (post.date.getFullYear() == year && post.date.getMonth() == month))

  poet.helpers.postsByAuthor = (email) ->
    poet.cache.postsByAuthor = poet.cache.postsByAuthor || {}

    if(poet.cache.postsByAuthor[email])
      console.debug("Posts by %s found in the cache", email)
      return poet.cache.postsByAuthor[email]

    console.debug("Retrieving posts by %s...", email)
    return poet.cache.postsByAuthor[email] = poet.helpers.getPosts(poet).filter((post) -> (post.author.email == email))

  poet.helpers.authorURL = (author) -> "/blog/authors/#{author.id ? author}"

  # Add the new methods to the express locals (to be able to use them on templates)
  poet.app.locals(poet.helpers)

  # Decorate md converter to process {% gist} marks
  mdConverter = poet.templates.md
  poet.addTemplate({
    ext: 'md',
    fn :  (s) -> mdConverter(s.replace(/\{%\s*gist\s*(.*?)\s*%\}/g, '<script src="https://gist.github.com//$1.js"></script>'))
  })


exports.decorateData = (poet) ->

  ###
  # Process the posts, replacing the author email with the "member" instance.
  ###
  poet.helpers.getPosts().forEach ( (post) ->
    
    post.imagePath = "posts/#{moment(post.date).format('YYYY-MM-DD')}-#{post.slug}/"

    post.bg = if post.bg then post.imagePath + post.bg else db.getData().config.blog.defaultBackgroundImage

    if(post.preview.indexOf("<p>") == 0)
      post.preview = post.preview.substring(3,post.preview.length-5)


    if(post.author && !post.author.email)
      team = db.getData().team
      author = _.find(team, (person) -> person.email == post.author)
      if(!author)
        author = {id: team.length, email: post.author, name: post.author, hide: true, photo: "person-icon.png"}
        team.push(author)

      post.author = author
  )