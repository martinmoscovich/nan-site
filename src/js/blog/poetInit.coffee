poetExtender = require('./poetExtension')
Poet = require 'poet'
poet = {}

exports.start = (app, routes) ->
  poet = Poet(app, {
    postsPerPage: 3,
    posts: './_posts',
    metaFormat: 'json',
    showFuture: false,
    readMoreLink : (post) -> ''
    routes: routes
  })

  poetExtender.addDefinitions poet

  poet.watch ->
    poetExtender.decorateData poet
    console.info("Posts updated")

  return exports.reload()

exports.reload = () ->
  return poet.init( (err, poet) ->
    if(err)
      console.error("Error initializing Poet", err)
      throw err
    else
      poetExtender.decorateData poet
      console.info("Poet service ready")
      return poet
  )