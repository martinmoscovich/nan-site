_ = require 'underscore'
moment = require 'moment'
db = require '../util/db'

module.exports = (app) ->
  app.locals({
    renderText: (paragraphs, tag = "p") ->
      if(_.isArray(paragraphs))
        fn = (result, value) -> result + "<#{tag}>#{value}</#{tag}>"
        return paragraphs.reduce fn, ""
      else
        return "<#{tag}>#{paragraphs}</#{tag}>"

    formatDate: (date, format = 'D MMM YYYY') -> moment(date).format(format)

    getSocialLinks: () -> db.getData().social

    getConfig: () -> db.getData().config
  })