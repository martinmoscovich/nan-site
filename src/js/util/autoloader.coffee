fs = require 'fs'
path = require 'path'

exports.load = (dir) -> doLoad dir, require

exports.loadJSON = (dir) -> doLoad dir, (file) -> JSON.parse(fs.readFileSync(file, 'utf8'))

doLoad = (dir, fn) ->
  modules = {}

  fs.readdirSync(dir).forEach (file) ->
    modules[file.split('.')[0]] = fn(path.join(dir, file))

  return modules


