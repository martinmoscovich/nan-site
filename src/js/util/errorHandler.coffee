exports.log = (err, req, res, next) ->
  console.error "Ocurrio un error", err
  next err


exports.restErrorHandler = (err, req, res, next) ->
  if req.xhr
    res.send 500, { error: 'Internal Server Error!' }
  else
    next err

exports.webErrorHandler = (err, req, res, next) ->
  res.status 500

  res.render 'pages/error'