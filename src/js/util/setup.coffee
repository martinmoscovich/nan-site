express = require 'express'
path = require 'path'
db = require './db'
routes = require '../routes'
errorHandler = require './errorHandler'
blog = require '../controllers/blog'

exports.init = ->
  require('econsole').enhance(db.getData().config.logLevel)
  console.debug 'Data loaded from files'

exports.configApp = (rootPath, app) ->
  blog.init app, routes.poetBlog

  ## all environments
  app.set('port', process.env.VCAP_APP_PORT || process.env.PORT || 3000)
  app.set('views', path.join(rootPath, 'src/views'))
  app.set('view engine', 'jade')
  app.use(express.favicon())

  if db.getData().config.logLevel == 'DEBUG'
    app.use(express.logger('dev'))

  app.use(express.static(path.join(rootPath, 'public/static')))
  app.use(require('less-middleware')(path.join(rootPath, '/src/less'),{dest: path.join(rootPath, '/public/generated'), preprocess: { path: (pathname) -> return pathname.replace(path.sep + 'css', '')}}, {compress: true }))
  app.use(express.static(path.join(rootPath, 'public/generated')))


  app.use(express.json())
  app.use(express.urlencoded())
  app.use(express.methodOverride())

  app.use ( req, res, next) ->
    res.locals.currentUrl = req.protocol + '://' + req.get('host') + req.originalUrl
    next()

  app.use(app.router)
  routes.site app

  require('./templateGlobals')(app)

  # Error handling
  app.use errorHandler.log
  app.use errorHandler.restErrorHandler
  app.use errorHandler.webErrorHandler
  ## development only
  #if 'development' == app.get('env')
  #  app.use(express.errorHandler())