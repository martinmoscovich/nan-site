fs = require 'fs'
path = require 'path'
autoloader = require './autoloader'
watch = require 'node-watch'
poetManager = require '../blog/poetInit'
_ = require 'underscore'

data = {}

data_dir = path.join(__dirname, '../../../data')

watch data_dir, (filename) ->
  console.info "Data file '%s' changed", filename
  load()
  poetManager.reload()
  console.info "Data reloaded"

generateIds = (list) ->
  list.forEach (item, i) -> item.id = i+1

findById = (id) -> _.findWhere(this, {id: parseInt(id)})

load  = ->
  data = autoloader.loadJSON data_dir
  generateIds(data.team)
  generateIds(data.projects)
  data.team.findById = findById
  data.projects.findById = findById

load()

exports.getData = -> data

exports.getById = (list, id) -> _.find(list, (item) -> item.id == id)