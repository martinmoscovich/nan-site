autoloader = require './util/autoloader'
controllers = autoloader.load __dirname + '/controllers'

###
# Standard express site routes and controllers
###
exports.site = (app) ->
  app.get '/', controllers.site.home
  app.get '/team', controllers.site.team
  app.get '/projects', controllers.site.projects
  app.get '/projects/:id', controllers.site.projectDetails
  app.get '/services', controllers.site.services
  app.get '/blog/authors/:author', controllers.blog.byAuthor

###
# Blog routes and associated views
###
exports.poetBlog = {
  '/blog/posts/:post': 'pages/blog/post'
  #'/blog/tags/:tag': 'pages/blog/tag',
  #'/blog/categories/:category': 'pages/blog/category'
}
