class WorkViewModel
  constructor: (state) ->
    @projects = ko.observableArray(state.list)
    @page = ko.observable(state.page)
    @next = ko.observable(state.next)
    @loading = ko.observable(false)
    @nextPage = ko.computed((() -> @page()+1), this)


$(document).ready () ->

  scrollToEnd = () ->
    $('body').animate({
      scrollTop: loadMore.offset().top - ($(window).height() - loadMore.height()) + 20
    }, 400)

  outerHtml = (elem) -> elem.clone().wrap('<p>').parent().html()
  firstChild = (elem) -> elem.find(":first-child")

  restoreState = () ->
    if sessionStorage
      return JSON.parse sessionStorage.getItem("projects")

  saveState = (list, page, next) ->
    if sessionStorage
      sessionStorage.setItem("projects", JSON.stringify({list: list, page: page, next: next}))


  state = restoreState()

  vm = new WorkViewModel(state ? window.nan.model)
  loadMore = $("#loadmore")
  list = $(".work-list-container")

  loadMore.click (e) ->
    load = $(this)
    e.preventDefault()
    vm.loading(true)

    setTimeout (() ->
      $.ajax(loadMore.attr('href'))
      .done((data) ->
        vm.projects.push.apply(vm.projects, data.items)
        vm.page(data.number)
        vm.loading(false)

        scrollToEnd()

        vm.next(data.next)

        saveState(vm.projects(), data.number, data.next)

      )), 500


  template = outerHtml firstChild(list)
  list.html(template)

  ko.applyBindings(vm)
