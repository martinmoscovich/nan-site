express = require 'express'

http = require 'http'
config = require './src/js/util/setup'

config.init()

app = express()
config.configApp __dirname, app

http.createServer(app).listen app.get('port'), ->
  console.log "NaN Labs site running on port  #{app.get('port')}"
