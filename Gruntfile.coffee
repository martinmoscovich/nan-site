module.exports = (grunt) ->

  serverPort = 9000
  livereloadPort = 35729
  OUTPUT_CLIENT = 'public/generated'
  BOWER_DIR = "libs/bower"

  dependencies =
    js: [
      'libs/modernizr.custom.js',
      'libs/knockout-3.1.0.js',
      'libs/headertransition.js',
      "#{BOWER_DIR}/bootstrap/dist/js/bootstrap.min.js",
      "#{BOWER_DIR}/jquery/dist/jquery.min.js"
      "#{BOWER_DIR}/jquery-waypoints/waypoints.min.js"
    ],
    fonts: [
      "#{BOWER_DIR}/font-awesome/fonts/*",
      "#{BOWER_DIR}/bootstrap/dist/fonts/*"
    ]


  ## Project configuration.
  grunt.initConfig
  ## Metadata.
    pkg: grunt.file.readJSON 'package.json'

    banner: '/*! <%= pkg.name %> - v<%=pkg.version%> - ' +
    '<%= grunt.template.today("yyyy-mm-dd, h:MM:ss TT") %>\n' +
    '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
    '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;*/\n'

    ## Task configuration
    clean:
      options: force: true
      dist: [OUTPUT_CLIENT]

    coffee:
      client:
        files: [{dest:"#{OUTPUT_CLIENT}/js", src: '*.coffee', expand: true, ext: '.js', cwd: 'src/client'}]

    copy:
      js:
        files: [{expand: true, flatten: true, src: dependencies.js, dest: "#{OUTPUT_CLIENT}/js", filter: 'isFile'}]
      fonts:
        files: [{expand: true, flatten: true, src: dependencies.fonts, dest: "#{OUTPUT_CLIENT}/fonts", filter: 'isFile'}]

    coffeelint:
      options:
        max_line_length: level: 'ignore'
      server: 'src/js/**/*.coffee'
      client: 'src/client/**/*.coffee'
      gruntfile: 'Gruntfile.coffee'

    watch:
      options:
        livereload:
          port: livereloadPort
        spawn: false
      gruntfile:
        files: 'Gruntfile.coffee'
        tasks: ['coffeelint:gruntfile']
      server:
        files: '<%= coffeelint.server.src %>'
        tasks: ['coffeelint:server', 'express:dev']
        options:
          livereload: true
      client:
        files: '<%= coffeelint.client.src %>'
        tasks: ['coffeelint:client', 'coffee:client']
        options:
          livereload: true
      views:
        files: 'src/views/**/*.jade'
        options:
          livereload: true

    express:
      dev:
        options:
          background: true
          script: 'app.coffee'
          delay: "1000"
          opts: ['./node_modules/coffee-script/bin/coffee']

  require('load-grunt-tasks')(grunt);

  grunt.registerTask 'build-client', ['coffeelint:client', 'coffee:client']
  grunt.registerTask 'build-server', ['coffeelint:server']
  grunt.registerTask 'build', ['clean', 'build-server', 'build-client', 'copy']
  grunt.registerTask 'default', ['build', 'express:dev', 'watch']