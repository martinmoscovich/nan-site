{{{
    "title"    : "Models in AngularJS",
    "author"   : "matias@nan-labs.com",
    "tags"     : [ "JavaScript", "AngularJS", "MVC" ],
    "category" : "AngularJS",
    "date"     : "11-18-2013",
    "preview"  : "Describes an approach to develop model objects in an AngularJS application. In this way AngularJS controllers are clean, object oriented and testables. It tries to fulfill the lack of information about this topic in the AngularJS official documentation."
}}}

##Introduction

One of the first challenges I’ve faced using <a target="_blank" href="http://angularjs.org">AngularJS</a> was *how to develop and handle my models inside the AngularJS controllers*. After reading the <a target="_blank" href="http://docs.angularjs.org">official AngularJS documentation</a>, it wasn’t clear at all how to write them. The examples don't show how to keep them organised to build large front-end (real) applications, because all the model data is declared inside controllers scope.

If a controller is in charge of handling a tiny model, with just a couple of variables, the <a target="_blank" href="http://code.angularjs.org/1.2.4/docs/guide/concepts#adding-ui-logic-controllers">example</a> (*check the invoice1.js file*) in AngularJS documentation seems to be the right and the easiest way to do it. But when our models start growing, having dozens of variables and business logic, things start to get messy, difficult to maintain and impossible to unit-test.

**The goal of this post is to describe how** I've managed **to do it**, **in a clean, object oriented and testable way**. At first, I'll talk a little bit about the problems I've found following the AngularJS documentation examples. And then, I'll introduce the proposed approach which allows us to:
- Write re-usable model objects,
- Develop clean controllers which are just in charge of orquestrate other components and
- Keep clean html partials, since <a target="_blank" href="http://code.angularjs.org/1.2.4/docs/guide/scope">they share scopes with controllers</a>.

##"Ugly" Approach

Let's suppose we need to develop a form, which it's used to create a new instance of one of our models (ex. a Contact from our Agenda). To develop this form, it seems enough to create two model variables inside the controller, and when the user presses the save button, call the service to save the new entity.

Here's the form controller code. Or you can check the complete <a target="_blank" href="http://jsfiddle.net/matiasalvarez87/xB6kV/2/">example</a>.

{% gist 7945265 %}

This is a simple controller, but it's easy to see a potential problem, the controller has a method called **getFullName()**. If we need to display the contact name in another section of the agenda, we'll be duplicating code (business logic).

Also, things get harder in our form's controller when we have to:
- Watch variables
- Hide/Show UI elements depending on the model state
- Validate form values (using client or server side validations)
- Use custom AngularJS directives

###Main Disadvantages:
- **Huge controller files** (which means <a target="_blank" href="http://en.wikipedia.org/wiki/Cohesion_(computer_science)">low cohesion</a>) containing:
	- Model code,
	- business logic,
	- helper methods to use in the UI,
	- calls to services,
	- variable watches.
- You **can't reuse model-related code** in other controllers.
- It's **harder to unit test** controllers than <a target="_blank" href="http://odetocode.com/blogs/scott/archive/2012/02/27/plain-old-javascript.aspx">plain old JS objects</a>. You have to inject/mock all the angular dependencies the controller use.


##Proposed Approach

Even though this is not a revolutionary approach, when I started to work with AngularJS I didn't realize the importance of have well defined models. After reading all the <a target="_blank" href="http://code.angularjs.org/1.2.4/docs/guide">official documentation</a>, it seems that is not an important thing to have in mind.

The first pages I developed were simple enough to follow the AngularJS examples, but when I started developing a big form controller, after some time and new requirements, the code was really ugly. So, I had to refactor it, to keep it well organized, maintainable, and easy to test.

### Models and Controllers

I believe **the ideal scenario is to keep our controllers as clean and simple as we can**. *The basic idea is to have an object which represents our model entity.*
**Controllers should JUST be in charge of**:

- Instantiating model instance/s.
- Calling services to perform back-end operations (with the model instance as an argument).
- Watching model fields and triggering actions depending on their changes.
- Calling model methods.

### Partials

At the same time, **this approach also help us to keep our partials** (or HTML templates) **clean**. Through the *scope*, partials can use model variables or call model methods to:
- Bind to model variables *(line 4)*
- Check or unckeck input elements calling a method *(line 7)*
- Hide or show a html elements *(line 9)*
- Change validation rules based on the model state *(line 14)*

{% gist 7946008 %}

###Developing models

As many of you know, there are several ways to work with classes and objects in JavaScript. If you don't know about it, maybe is better to read <a target="_blank" href="http://www.phpied.com/3-ways-to-define-a-javascript-class/">some information</a>.
Since I'm a huge fan of <a target="_blank" href="http://underscorejs.org">UnderscoreJS</a> *(I think it's one of those libraries which is a MUST have in any serious JavaScript application)*, I'll use it to show an example of how to develop model classes using this library.

For example lets see the Contact model class for our Agenda example:

{% gist 7993643 %}

- At first we define the class constructor usign some default values.
- Then we define all the class state methods and helpers.
- Return the class definition.

Advantages:
- Model methods can be re-used in different controllers (e.g. getFullName() method).
- JS objects are easy to unit test, since there is no need to add or mock dependencies. Business logic is the core of our application, so model classes are the <a target="_blank" href="http://joelhooks.com/blog/2013/04/24/modeling-data-and-state-in-your-angularjs-application/">"prime candidates for unit testing"</a>.
- The business logic related to contacts is not scattered among different controllers.

###Light and nice controllers

The form controller code to create and edit contacts is small and simple:

{% gist 8043566 %}

##Conclusion

A nice to have would be a JS abstract class which would have all the common methods for defining model classes. In this way we would not duplicate the code.

I've created a mini AngularJS application (Agenda) to show how to use model classes with controllers, partials and services. This is the link to the github repository: <a target="_blank" href="https://github.com/matiasalvarez87/angular-agenda">https://github.com/matiasalvarez87/angular-agenda</a>. You can also check out the <a target="_blank" href="http://matiasalvarez87.github.io/angular-agenda/">live demo</a>.

IMHO, I believe this approach allows us to have more organised AngularJS applications. Each kind of component is just in charge of a specific task. Right now, I'm using this approach to build a large AngularJS application and so far I haven't had complex issues with it.

Please feel free to comment, share, ask or criticize. I'd like to hear thoughts about this topic and alternative ways to resolve the same problem.